import express, {Express, Response, Request} from 'express'
import { GoodbyeController } from '../controller/GoodbyeController';
import { LogInfo } from "../utils/logger";

// Router from express
let goodbyeRouter = express.Router();

goodbyeRouter
    .route('/')
    .get(async (req: Request, res: Response) => {
        let name: any = req?.query?.name;

        LogInfo(`Query Params: ${name}`)
        const controller: GoodbyeController = new GoodbyeController();

        const response = await controller.getGoodbye(name)

        return res.send(response)
    })

export default goodbyeRouter;