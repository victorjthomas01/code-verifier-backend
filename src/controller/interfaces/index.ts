import { BasicResponse, ExtendedResponse } from "../types";

export interface IHelloController {
  getMessage(name?: string): Promise<BasicResponse>;
}

export interface IGoodbyeController {
  getGoodbye(name?: string): Promise<ExtendedResponse>;
}