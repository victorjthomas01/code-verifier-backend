import { ExtendedResponse } from "./types";
import { IGoodbyeController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

export class GoodbyeController implements IGoodbyeController {
  public async getGoodbye(name?: string | undefined): Promise<ExtendedResponse> {
    const date = new Date().toJSON();
    LogSuccess("[/api/goodbye] Get Request");

    return {
      message: `Goodbye, ${name}`,
      Date: `${date}`,
    };
  }
}
